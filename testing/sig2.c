#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned int uint32_t;
typedef struct counters_t counters_t;
struct counters_t {
    uint32_t total;
    struct timeval t;
    float avg;
    long int sum;
    long int sum2;
    float sig2;
    uint32_t lengtherror;
    uint32_t usec_0_125;
    uint32_t usec_125_250;
    uint32_t usec_250_500;
    uint32_t usec_500_750;
    uint32_t usec_750_1000;
    uint32_t usec_1000;
};

void main() {
    counters_t cnt;
    int i;
    int tab[] = {9, 2, 5, 4, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4};
    for (i = 0; i < 20; i++) {
        long int lat = tab[i];
        int n = ++cnt.total;
        cnt.sum += lat;
        cnt.avg = (float)(cnt.sum) / (float)n;
        cnt.sum2 += lat * lat;
        cnt.sig2 = (float)cnt.sum2 / (float)n - (cnt.avg * cnt.avg);
    }
    printf("Avg=%.2f, Sig2=%.2f\n", cnt.avg, cnt.sig2);
}
