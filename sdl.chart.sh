# no need for shebang - this file is loaded from charts.d.plugin
# SPDX-License-Identifier: GPL-3.0+

# netdata
# real-time performance and health monitoring, done right!
# (C) 2016 Costa Tsaousis <costa@tsaousis.gr>
#

# if this chart is called X.chart.sh, then all functions and global variables
# must start with X_

# _update_every is a special variable - it holds the number of seconds
# between the calls of the _update() function
sdl_update_every=

# the priority is used to sort the charts on the dashboard
# 1 = the first chart
sdl_priority=999

# to enable this chart, you have to set this to 12345
# (just a demonstration for something that needs to be checked)
sdl_magic_number=12345

# global variables to store our collected data
# remember: they need to start with the module name sdl_

sdl_value=
sdl_mean=
sdl_variance=
sdl_min=
sdl_max=
sdl__0_15=
sdl__15_25=
sdl__25_50=
sdl__50_75=
sdl__75_100=
sdl__100_500=
sdl__500_1000=
sdl__1000p=
sdl_mm_1k=
sdl_mm_10k=
sdl_mm_100k=
sdl_mv_1k=
sdl_mv_10k=
sdl_mv_100k=

sdl_timer_setrow=
sdl_real_setrow=

sdl_last=0
sdl_count=0

sdl_get() {
	# do all the work to collect / calculate the values
	# for each dimension
	#
	# Remember:
	# 1. KEEP IT SIMPLE AND SHORT
	# 2. AVOID FORKS (avoid piping commands)
	# 3. AVOID CALLING TOO MANY EXTERNAL PROGRAMS
	# 4. USE LOCAL VARIABLES (global variables may overlap with other modules)

	stats="`cat /tmp/consumer_0 | sed -e "s/\.//g"`"

	sdl_value=`echo $stats | cut -d ' ' -f 1`
	sdl_mean=`echo $stats | cut -d ' ' -f 2`
	sdl_variance=`echo $stats | cut -d ' ' -f 3`
	sdl_mm_1k=`echo $stats | cut -d ' ' -f 4`
	sdl_mv_1k=`echo $stats | cut -d ' ' -f 5`
	sdl_mm_10k=`echo $stats | cut -d ' ' -f 6`
	sdl_mv_10k=`echo $stats | cut -d ' ' -f 7`
	sdl_mm_100k=`echo $stats | cut -d ' ' -f 8`
	sdl_mv_100k=`echo $stats | cut -d ' ' -f 9`

        sdl_min=`echo $stats | cut -d ' ' -f 10`
        sdl_max=`echo $stats | cut -d ' ' -f 11`

        sdl_0_15=`echo $stats | cut -d ' ' -f 12`
        sdl_15_25=`echo $stats | cut -d ' ' -f 13`
        sdl_25_50=`echo $stats | cut -d ' ' -f 14`
        sdl_50_75=`echo $stats | cut -d ' ' -f 15`
        sdl_75_100=`echo $stats | cut -d ' ' -f 16`
        sdl_100_500=`echo $stats | cut -d ' ' -f 17`
        sdl_500_1000=`echo $stats | cut -d ' ' -f 18`
        sdl_1000p=`echo $stats | cut -d ' ' -f 19`


	stats_producer="`cat /tmp/producer_0 | sed -e "s/\.//g"`"
	sdl_timer_setrow=`echo $stats_producer | cut -d ' ' -f 1`
        sdl_real_setrow=`echo $stats_producer | cut -d ' ' -f 2`

	# this should return:
	#  - 0 to send the data to netdata
	#  - 1 to report a failure to collect the data

	return 0
}

# _check is called once, to find out if this chart should be enabled or not
sdl_check() {
	# this should return:
	#  - 0 to enable the chart
	#  - 1 to disable the chart

	# check something
	[ "${sdl_magic_number}" != "12345" ] && error "manual configuration required: you have to set sdl_magic_number=$sdl_magic_number in example.conf to start example chart." && return 1

	# check that we can collect data
	sdl_get || return 1

	return 0
}

# _create is called once, to create the charts
sdl_create() {

	cat <<EOF
CHART sdl.producer0_setrow '' "setrow() function call rate" "interval [us]" Setrow_Rate_(x3_burst) SDL Area $((sdl_priority + 1)) $sdl_update_every
DIMENSION timer_setrow '' absolute 1 100
DIMENSION real_setrow '' absolute 1 100

CHART sdl.consumer0_latency '' "Producer to Consumer Latency" "latency [us]" Latency SDL Area $((sdl_priority + 2)) $sdl_update_every
DIMENSION value '' absolute 1 100
DIMENSION mean '' absolute 1 100
DIMENSION variance '' absolute 1 100
DIMENSION mm_1k '' absolute 1 100
DIMENSION mv_1k '' absolute 1 100
DIMENSION mm_10k '' absolute 1 100
DIMENSION mv_10k '' absolute 1 100
DIMENSION mm_100k '' absolute 1 100
DIMENSION mv_100k '' absolute 1 100

CHART sdl.consumer0_percentile '' "Percentile" "counts [%]" Percentile SDL Area $((sdl_priority + 3)) $sdl_update_every
DIMENSION _0_15 '' absolute 1 100
DIMENSION _15_25 '' absolute 1 100
DIMENSION _25_50 '' absolute 1 100
DIMENSION _50_75 '' absolute 1 100
DIMENSION _75_100 '' absolute 1 100
DIMENSION _100_500 '' absolute 1 100
DIMENSION _500_1000 '' absolute 1 100
DIMENSION _1000p '' absolute 1 100

CHART sdl.consumer0_peaklatency '' "Peak Latency" "peak latency [us]" Peak SDL Area $((sdl_priority + 4)) $sdl_update_every
DIMENSION max '' absolute 1 100
DIMENSION min '' absolute 1 100

EOF

	return 0
}

# _update is called continuously, to collect the values
sdl_update() {
	# the first argument to this function is the microseconds since last update
	# pass this parameter to the BEGIN statement (see bellow).

	sdl_get || return 1

	# write the result of the work.
#SET min = $sdl_min
#SET max = $sdl_max


	cat <<VALUESEOF
BEGIN sdl.producer0_setrow $1
SET timer_setrow = $sdl_timer_setrow
SET real_setrow = $sdl_real_setrow
END

BEGIN sdl.consumer0_latency $1
SET value = $sdl_value
SET mean = $sdl_mean
SET variance = $sdl_variance
SET mm_1k = $sdl_mm_1k
SET mv_1k = $sdl_mv_1k
SET mm_10k = $sdl_mm_10k
SET mv_10k = $sdl_mv_10k
SET mm_100k = $sdl_mm_100k
SET mv_100k = $sdl_mv_100k
END

BEGIN sdl.consumer0_peaklatency $1
SET max = $sdl_max
SET min = $sdl_min
END

BEGIN sdl.consumer0_percentile $1
SET _0_15 = $sdl_0_15
SET _15_25 = $sdl_15_25
SET _25_50 = $sdl_25_50
SET _50_75 = $sdl_50_75
SET _75_100 = $sdl_75_100
SET _100_500 = $sdl_100_500
SET _500_1000 = $sdl_500_1000
SET _1000p = $sdl_1000p
END

VALUESEOF

	return 0
}
