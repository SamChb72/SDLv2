#!/bin/bash

rm -rf bin
mkdir -p bin
cp ../bin/* ./bin/.
docker build -t sdl_docker .
docker run -it sdl_docker /bin/ls /sdl/bin

