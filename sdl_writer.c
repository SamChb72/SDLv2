//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#              samy.chbinou@greenflows.fr              #
//########################################################

#include "sdl.h"
#include <netdb.h>

int topic_write(unsigned char topicId, char *msg, int nmsg, int itvl) {
    char current[64];
    char log[64];
    bzero(current, sizeof(current));
    bzero(log, sizeof(log));

    /* prepare payload and write to shared memory */
    struct timeval t0;
    int i;
    for (i = 0; i < nmsg; i++) {
        gettimeofday(&t0, NULL);
        sprintf(current, "/sdl/%c/current", topicId);
        sprintf(log, "/sdl/%c/log", topicId);
        FILE *cfd = fopen(current, "w");
        if (0 > cfd) {
            perror("open()");
            return EXIT_FAILURE;
        }
        FILE *lfd = fopen(log, "a");
        if (!lfd) {
            perror("fopen()");
            return EXIT_FAILURE;
        }

        char buffer[BUFSIZE];
        bzero(buffer, sizeof(buffer));
        if (0 > fprintf(cfd, "%c:%ld:%s", topicId, (t0.tv_sec) * (int)1e6 + (t0.tv_usec), msg)) {
            perror("fprintf()");
        }
        if (0 > fprintf(lfd, "%c:%ld:%s\n", topicId, (t0.tv_sec) * (int)1e6 + (t0.tv_usec), msg)) {
            perror("fprintf()");
        }
        // fflush(cfd);
        // fflush(lfd);
        // usleep(itvl);

        fclose(cfd);
        fclose(lfd);
        usleep(itvl);
    }
    return 0;
}

int main(int argc, char **argv) {
    char *topic;
    char *msg;

    /* check command line arguments */
    if (argc != 5) {
        fprintf(stderr, "usage: %s <topic> <message> <nb_messages> <usec_interval>\n", argv[0]);
        exit(0);
    }
    topic = argv[1];
    msg = argv[2];

    /* check user parameters */

    /* topic */
    if (strlen(topic) > 64) {
        printf("Please use a topic name less than 64 characters long\n");
        exit(EXIT_FAILURE);
    }

    /* msg */
    if (strlen(msg) > 512) {
        printf("Please send messages less than 512 characters long\n");
        exit(EXIT_FAILURE);
    }

    /* build the server's Internet address */
    unsigned char topicId = topic_id(topic);
    int ret = topic_write(topicId, msg, atoi(argv[3]), atoi(argv[4]));
    while (EXIT_FAILURE == ret) {
        // Retry every second
        usleep(1000 * 1000);
        ret = topic_write(topicId, msg, atoi(argv[3]), atoi(argv[4]));
    }
}
