########################################################
#       Copyright (C) 2018 GreenFLOWS                  #
#                                                      #
# This file is part of GreenFLOWS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOWS      #
#                                                      #
#             samy.chbinou@greenflows.fr               #
########################################################

#!/bin/bash

CCOPTS="-O3 -Wall -Werror"
#CCOPTS="-g -Wall -Werror"

bindir=bin

# Clean objects, binaries, ident code
./clean.sh

#
# Compile server, producer and consumer
#
mkdir -p $bindir
echo "BUILD ECHO TCP SERVER & CLIENT"
gcc $CCOPTS -o $bindir/echo_tcp_server echo_tcp_server.c -lm && gcc $CCOPTS -o $bindir/echo_tcp_client echo_tcp_client.c -lm 

echo "BUILD TCP BROCKER & CLIENTS"
gcc $CCOPTS -o $bindir/sdl_tcp_brocker sdl_tcp_brocker.c -lm && gcc $CCOPTS -o $bindir/sdl_tcp_producer sdl_tcp_producer.c -lm && gcc $CCOPTS -o $bindir/sdl_tcp_consumer sdl_tcp_consumer.c -lm

echo "BUILD ECHO UDP SERVER & CLIENT"
gcc $CCOPTS -o $bindir/echo_udp_server echo_udp_server.c -lm && gcc $CCOPTS -o $bindir/echo_udp_client echo_udp_client.c -lm

echo "BUILD UDP BROCKER & CLIENTS"
gcc $CCOPTS -o $bindir/sdl_udp_brocker sdl_udp_brocker.c -lm && gcc $CCOPTS -o $bindir/sdl_udp_producer sdl_udp_producer.c -lm && gcc $CCOPTS -o $bindir/sdl_udp_consumer sdl_udp_consumer.c -lm

echo "BUILD ECHO RAW SERVER & CLIENT"
gcc $CCOPTS -o $bindir/echo_raw_server echo_raw_server.c -lm && gcc $CCOPTS -o $bindir/echo_raw_client echo_raw_client.c -lm

echo "BUILD RAW BROCKER & CLIENTS"
echo "BUILD MQ BROCKER & CLIENTS"

echo "STRIPPING BINARIES"
cd $bindir && strip * && cd ..

echo "DONE"
