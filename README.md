# Shared Data Layer - SDL

![sdl-image](http://greenflows.fr/img/sdl/sdl.png)

Shared Data Layer is a networking memory space that is common to many microservices from cloud native 5G components and it is used by theses microservices to exchange data between them.
Principle is based on Get() and Set() values as messages into memory pipelines.
With Set() usage function, the microservice writes a message containing values into a named pipeline. In that case this microservice acts like a producer.
With Get() usage function, the microservice reads the message stored in the pipeline. In that case this microservice acts like a consumer.
Software architecture must have enough flexibility to permit communication between different hardware configurations (same host or not, same network or not).

Microservices can be producers or/and consumers and thy share the data through registered brocker pipelines:

![sdl-image](http://greenflows.fr/img/sdl/prodbrockcons.png)

Microservices can be on hosts on different subnets:

![sdl-image](http://greenflows.fr/img/sdl/differentSubnets.png)

Or connected with a switch on same network:

![sdl-image](http://greenflows.fr/img/sdl/withSwitch.png)

### Setting the clock
For this low latency configuration and this level of precision (few microsecond) we must use Precision Time Protocol IEEE-1588.
![sdl-image](http://greenflows.fr/img/sdl/ptpconf.png)

Three permanent services are started on brocker: two ptp4l services to set master clock on port0 and port1 and one phc2sys service to set system clock:
![sdl-image](http://greenflows.fr/img/sdl/ptpbrocker1.png)

![sdl-image](http://greenflows.fr/img/sdl/ptpbrocker2.png)

![sdl-image](http://greenflows.fr/img/sdl/ptpbrocker3.png)

### SDL api
![sdl-image](http://greenflows.fr/img/sdl/sdlapi.png)

### Main script ./build.sh does many steps automatically:
1. git clone source code
1. compile source code
1. pack binaries into .deb package
1. build small ubuntu docker image with debian package installed
1. run the docker image

![sdl-image](http://greenflows.fr/img/sdl/docker-sdl.png)

### Usage:
1. Isolate few cores from the global linux system for each sdl binaries with kernel option isolcpus="1-3"
1. Start a brocker `VMA_SPEC=latency VMA_THREAD_MODE=3 LD_PRELOAD=libvma.so taskset -c 1 ./bin/sdl_udp_brocker INADDR_ANY 65111`
1. Start a consumer on core 1 (for the get) and 2 (for the listen) `LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/samy/SDLv2/lib VMA_SPEC=latency VMA_THREAD_MODE=3 LD_PRELOAD=libvma.so CONSUMER_CPU=2 taskset -c 1 ./bin/poc_sdl_consumer_exemple 192.168.20.10`
1. Start publisher on core 2 `LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/samy/SDLv2/lib VMA_SPEC=latency VMA_THREAD_MODE=3 LD_PRELOAD=libvma.so taskset -c 2 ./bin/poc_sdl_producer_exemple 192.168.30.10`
1. open you netdata browser to see latencies graphs

![sdl-image](http://greenflows.fr/img/sdl/latencies.png)

![sdl-image](http://greenflows.fr/img/sdl/xtermconsumer.png)


### With docker:
1. brocker starts with `docker run --cpuset-cpus=1 --network=host -t --ulimit memlock=-1 --device=/dev/infiniband/uverbs0 --device=/dev/infiniband/uverbs1 --device=/dev/infiniband/rdma_cm sdl_docker sh -c 'VMA_SPEC=latency LD_PRELOAD=libvma.so /sdl/bin/sdl_udp_brocker INADDR_ANY 65111'`


### Installing kubernetes on two nodes: a master and a slave
On Master Node:
1. sudo apt-get update && sudo apt-get install -y apt-transport-https
2. sudo sh -c 'curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -'
3. sudo sh -c 'cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
'
4. sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl docker.io
5. sudo apt-mark hold kubelet kubeadm kubectl
7. sudo swapoff -a
8. comment swap from /etc/fstab
9. sudo kubeadm init --apiserver-advertise-address=192.168.1.242
10. mkdir -p $HOME/.kube
11. sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
12. sudo chown $(id -u):$(id -g) $HOME/.kube/config
13. kubectl apply -f https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/hosted/etcd.yaml
14. kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
15. kubectl get pods --all-namespaces

On Slave Node:
Instead of running kubeadm init run
1. sudo kubeadm join 192.168.1.242:6443 --token a6aj3a.m75rtrwttu1vk3go --discovery-token-ca-cert-hash sha256:4f4a72b459cbd8d1156a118d0ebffc628ba70484741a29d3da0b6312b3278cc7

