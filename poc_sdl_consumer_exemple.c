#include <math.h>
#include <signal.h>

#include "poc_sdl_client.h"

#define interval_nanosec 10000

consume_t *services;
timer_t timer_get;
char start_time_string[64];

typedef struct stats_comp_t stats_comp_t;
struct stats_comp_t {
    float mean;
    float variance;
    float mm_1k;
    float variance_1k;
    float mm_10k;
    float variance_10k;
    float mm_100k;
    float variance_100k;
    float bitrate;
};
stats_comp_t *stats_comp;

void start_timer_get(void) {
    struct itimerspec value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = (int)interval_nanosec;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = (int)interval_nanosec;
    timer_create(CLOCK_REALTIME, NULL, &timer_get);
    timer_settime(timer_get, 0, &value, NULL);
}

void stop_timers(void) {
    struct itimerspec value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = 0;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    timer_settime(timer_get, 0, &value, NULL);
}

void compute_stats(stats_t *stats) {
    stats_comp->mean = (float)(stats->sum) / (float)(stats->total);
    stats_comp->mm_1k = (float)(stats->sum_1k) / 1000.0;
    stats_comp->mm_10k = (float)(stats->sum_10k) / 10000.0;
    stats_comp->mm_100k = (float)(stats->sum_100k) / 100000.0;

    stats_comp->variance = sqrt((float)stats->sum2 / (float)(stats->total) - (float)(stats_comp->mean * stats_comp->mean));
    stats_comp->variance_1k = sqrt((float)stats->sum2_1k / 1000.0 - (float)(stats_comp->mm_1k * stats_comp->mm_1k));
    stats_comp->variance_10k = sqrt((float)stats->sum2_10k / 10000.0 - (float)(stats_comp->mm_10k * stats_comp->mm_10k));
    stats_comp->variance_100k = sqrt((float)stats->sum2_100k / 100000.0 - (float)(stats_comp->mm_100k * stats_comp->mm_100k));

    // stats_comp->bitrate = sum_size; // divided by duration of ... to be continued
}

void print(int row_id, char *buf, consume_t *services) {
    if (0 == row_id) {
        time_t current_time;
        char *c_time_string;
        current_time = time(NULL);
        c_time_string = ctime(&current_time);
        printf("%s\n" GRN "Test started at %sCurrent Table portion preview at %s\n" RESET, "-------------------------------------------------------------------------", start_time_string, c_time_string);
    }
    printf(GRN "%s\n" RESET, buf);
    if (ROW_NB - 1 == row_id) {
        stats_t stats;
        pthread_mutex_lock(&mutexes.mutex_consume);
        //        pthread_cond_signal(&mutexes.cond_consume);
        //        pthread_cond_wait(&mutexes.cond_display, &mutexes.mutex_consume);
        memcpy(&stats, &services->stats, sizeof(stats_t));
        pthread_mutex_unlock(&mutexes.mutex_consume);
        compute_stats(&stats);
        printf("\nSTATS: [μs]: N=%u latency=%u M=%.2f σ=%.2f MM_1k=%.2f σ_1k=%.2f MM_10k=%.2f σ_10k=%.2f MM_100k=%.2f σ_100k=%.2f min=%d max=%d size_errors=%u datarate=%.2f[Gb/s]\n", stats.total, stats.latency, stats_comp->mean,
               stats_comp->variance, stats_comp->mm_1k, stats_comp->variance_1k, stats_comp->mm_10k, stats_comp->variance_10k, stats_comp->mm_100k, stats_comp->variance_100k, stats.min, stats.max, stats.size_errors, 0.0);
        printf("Percentile distribution: _0_15: %.2f%% _15_25: %.2f%% _25_50: %.2f%%\n\n", (100.0 * stats.cnt._0_15 / (float)stats.total), (100.0 * stats.cnt._15_25 / (float)stats.total), (100.0 * stats.cnt._25_50 / (float)stats.total));
    }
}

void write_to_netdata(consume_t *services) {
    char filename[64];
    bzero(&filename, sizeof(filename));
    sprintf(filename, "/tmp/consumer_%d", services->id);
    FILE *f = fopen(filename, "w");
    if (f == NULL) {
        exit(1);
    }
    stats_t stats;
    pthread_mutex_lock(&mutexes.mutex_consume);
    //    pthread_cond_signal(&mutexes.cond_consume);
    //    pthread_cond_wait(&mutexes.cond_display, &mutexes.mutex_consume);
    memcpy(&stats, &services->stats, sizeof(stats_t));
    pthread_mutex_unlock(&mutexes.mutex_consume);

    int min = 0;
    if (999999 != stats.min)
        min = stats.min;
    fprintf(f, "%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n", (float)stats.latency, stats_comp->mean, stats_comp->variance, stats_comp->mm_1k, stats_comp->variance_1k, stats_comp->mm_10k,
            stats_comp->variance_10k, stats_comp->mm_100k, stats_comp->variance_100k, (float)min, (float)stats.max, 100.0 * (float)stats.cnt._0_15 / (float)stats.total, 100.0 * (float)stats.cnt._15_25 / (float)stats.total,
            100.0 * (float)stats.cnt._25_50 / (float)stats.total, 100.0 * (float)stats.cnt._50_75 / (float)stats.total, 100.0 * (float)stats.cnt._75_100 / (float)stats.total, 100.0 * (float)stats.cnt._100_500 / (float)stats.total,
            100.0 * (float)stats.cnt._500_1000 / (float)stats.total, 100.0 * (float)stats.cnt._1000p / (float)stats.total);
    fclose(f);
}

void display() {
    // printf("graphs update\n");
    write_to_netdata(services);

    static int itvl_nb = 0;
    if (5 == itvl_nb++) {
        itvl_nb = 0;
        char row[ROW_MAXLENGTH];
        printf("timer get_row(): every %dμs\n", (int)interval_nanosec / 1000);
        printf("Display first 5 rows from %d to %d and last 5 rows from %d to %d:\n", 0, 5, ROW_NB - 5, ROW_NB);
        for (int row_id = 0; row_id < 5; row_id++) {
            if ((services) && (services->table)) {
                bzero(&row, sizeof(row));
                sprintf(row, "%s", sdl_get_row(services->table, row_id));
                // print on xterm shell every 5 sec
                print(row_id, row, services);
            }
        }
        printf(GRN "...\n" RESET);
        for (int row_id = ROW_NB - 5; row_id < ROW_NB; row_id++) {
            if ((services) && (services->table)) {
                bzero(&row, sizeof(row));
                sprintf(row, "%s", sdl_get_row(services->table, row_id));
                // print on xterm shell every 5 sec
                print(row_id, row, services);
            }
        }
    }
}

void timer_get_callback(int sig) {
    // printf(" Catched timer signal: %d ... !!\n", sig);
    char row[ROW_MAXLENGTH];
    if ((services) && (services->table)) {
        bzero(&row, sizeof(row));
        sprintf(row, "%s", sdl_get_row(services->table, random() % ROW_NB));
    }

    static int itvl_display = 0;
    // display every second
    if ((int)1000000000 / (int)interval_nanosec == itvl_display++) {
        itvl_display = 0;
        display();
    }
}

int main(int argc, char **argv) {
    time_t start_time;
    start_time = time(NULL);
    bzero(&start_time_string, sizeof(start_time_string));
    memcpy(start_time_string, ctime(&start_time), sizeof(start_time_string));

    stats_comp = calloc(1, sizeof(stats_comp));
    services = sdl_consumer_start(argv[1], 65111, "MICRO_SERVICES_INFO");

    (void)signal(SIGALRM, timer_get_callback);
    start_timer_get();

    while (1)
        ;

    stop_timers();
    free(stats_comp);
    return 0;
}
