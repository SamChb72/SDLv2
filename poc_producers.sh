#!/bin/bash

ip=$1
port=$2

topics='UI_NS_VNF_ORCHESTRATION UI_NS_VNC_SETTING UI_SS_TRAFFIC_PROFILE'

for topic in $topics
do
	cmd="taskset -c 1 ./bin/poc_sdl_udp_producer $ip $port $topic 256 200000 0"
	echo $cmd
	$cmd &
	sleep 0.001
done

