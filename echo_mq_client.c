#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "sdl.h"

#define PAYLOAD_256                                                                                                                                                                                                                            \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" \
    "AAAAAAAAAAAAAAAAAAAAAAAA"

int main(int argc, char **argv) {
    mqd_t up, down;

    /* open the UpLink queue */
    up = mq_open(QUEUE_UP, O_WRONLY);
    CHECK((mqd_t)-1 != up);

    /* open the DownLink queue */
    down = mq_open(QUEUE_DOWN, O_RDONLY);
    CHECK((mqd_t)-1 != down);

    //    do {
    //    } while (strncmp(buffer, MSG_STOP, strlen(MSG_STOP)));

    int nbmsg = 1000000;
    int count = 0;
    counters_t counters;
    bzero(&counters, sizeof(counters_t));
    char hostname[32];
    bzero(&hostname, sizeof(hostname));
    gethostname(hostname, sizeof(hostname));

    counters.total = 0;
    for (count = 0; count < nbmsg; count++) {
        struct timeval t0;
        char sndbuf[BUFSIZE], rcvbuf[BUFSIZE];

        bzero(sndbuf, BUFSIZE);
        bzero(rcvbuf, BUFSIZE);
        gettimeofday(&t0, NULL);
        sprintf(sndbuf, "%c:%ld:%s", '1', (t0.tv_sec) * (int)1e6 + (t0.tv_usec), PAYLOAD_256);
        /* send the message */
        CHECK(0 <= mq_send(up, sndbuf, BUFSIZE, 0));

        /* receive the echo message */
        ssize_t bytes_read;
        bytes_read = mq_receive(down, rcvbuf, MAX_SIZE, NULL);
        CHECK(bytes_read >= 0);
        rcvbuf[bytes_read] = '\0';
        print_rcv_stats(hostname, &counters, rcvbuf);
    }
    mq_send(up, "", 0, 0);
    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(up));
    CHECK((mqd_t)-1 != mq_close(down));

    return 0;
}
