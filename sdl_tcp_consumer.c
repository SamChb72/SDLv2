//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#              samy.chbinou@greenflows.fr              #
//########################################################

#include "sdl.h"

int subscribe_receive(struct sockaddr_in serveraddr, char *topic, unsigned int msglen, counters_t *counters) {
    char buf[BUFSIZE];
    int nbytes;
    char host[32];
    bzero(&host, sizeof(host));
    gethostname(host, sizeof(host));

    /* socket: create the socket */
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("socket()");
        return EXIT_FAILURE;
    }

    // sockopt(sockfd, 19 + msglen);

    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) {
        perror("connect() to brocker");
        close(sockfd);
        return EXIT_FAILURE;
    }
    printf("Connected to brocker !!! Subscribing to %s\n", topic);

    /* Subscription */
    int flags = 0; // | MSG_NOSIGNAL;
    char tpid = topic_id(topic);
    nbytes = sendto(sockfd, &tpid, 1, flags, NULL, 0);
    // printf("n=%d\n", n);
    if (nbytes < 0) {
        printf("Subscription failed. ");
        perror("sendto()");
        close(sockfd);
        return EXIT_FAILURE;
    }

    printf("Waiting data from brocker\n");
    for (;;) {
        bzero(buf, BUFSIZE);
        int recvflags = O_NONBLOCK; // | MSG_DONTWAIT;
        nbytes = recvfrom(sockfd, buf, sizeof(buf), recvflags, NULL, NULL);
        if (nbytes < 0) {
            perror("recvfrom()");
            break;
        }
        // printf("rcv %s\n", buf);
        print_rcv_stats(host, counters, msglen, buf, 10000);
        if (19 + 4 == nbytes) {
            printf("RECEIVED STOP MSG\n");
            break;
        }
    }
    close(sockfd);
    return EXIT_FAILURE;
}

int main(int argc, char **argv) {
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    int portno;
    char *topic;
    unsigned int msglen;
    counters_t counters;
    bzero(&counters, sizeof(counters));

    /* check command line arguments */
    if (argc != 5) {
        fprintf(stderr, "usage: %s <hostname> <port> <topic> <msglen>\n", argv[0]);
        fprintf(stderr, "ex: %s 127.0.0.1 65111 MICRO_SERVICES_INFO 256\n", argv[0]);
        exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);
    topic = argv[3];
    msglen = atoi(argv[4]);

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        perror("gethostbyname()");
        exit(EXIT_FAILURE);
    }

    /* portno */
    if (portno <= 49151) {
        printf("This port number (%d) is reserved and cant be used.\nport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }
    if (portno >= 65535) {
        printf("This port number (%d) is too big.\nnport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }

    /* topic */
    if (strlen(topic) > 64) {
        printf("Please use a topic name less than 64 characters long\n");
        exit(EXIT_FAILURE);
    }

    /* build the server's Internet address */
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    int ret = subscribe_receive(serveraddr, topic, msglen, &counters);
    while (0 != ret) {
        // Retry every second
        usleep(1000 * 1000);
        ret = subscribe_receive(serveraddr, topic, msglen, &counters);
    }
    return EXIT_SUCCESS;
}
