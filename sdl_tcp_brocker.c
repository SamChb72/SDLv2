//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#             samy.chbinou@greenflows.fr               #
//########################################################

#include "sdl.h"

#define MAXEVENTS 1024

/* Display list */
void print_fdl(fdl_t *l) {
    while (l) {
        printf("%c:%d\n", l->topic, l->fd);
        l = l->next;
    }
}

/* Remove a socket descriptor from list */
fdl_t *rm_fd(fdl_t *l, int fd) {
    if (!l)
        return NULL;
    if (l->fd == fd) {
        fdl_t *tmpnext;
        tmpnext = l->next;
        close(l->fd);
        free(l);
        return tmpnext;
    }
    l->next = rm_fd(l->next, fd);
    return l;
}

/* Add a socket descriptor from list */
fdl_t *add_fd(fdl_t *l, fdl_t v) {
    fdl_t *new = (fdl_t *)malloc(sizeof(fdl_t));
    memset(new, 0, sizeof(fdl_t));

    new->fd = v.fd;
    new->topic = v.topic;
    new->next = l;

    return new;
}

static void set_nonblocking(int fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl()");
        return;
    }
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
        perror("fcntl()");
    }
}

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "usage: %s <brocker_ip> <brocker_port>\n", argv[0]);
        fprintf(stderr, "ex: %s 127.0.0.1 65111\n", argv[0]);
        exit(0);
    }
    char *ip_addr = argv[1];
    int port = atoi(argv[2]);
    /* port */
    if (port <= 49151) {
        printf("This port number (%d) is reserved and cant be used.\nport must be "
               "49151<port<65535",
               port);
        exit(EXIT_FAILURE);
    }
    if (port >= 65535) {
        printf("This port number (%d) is too big.\nnport must be 49151<port<65535", port);
        exit(EXIT_FAILURE);
    }

    fdl_t *fd_list = NULL;
    // create the server socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("socket()");
        return 1;
    }
    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) == -1) {
        perror("setsockopt()");
        close(sock);
        return 1;
    }
    // sockopt(sock);
    set_nonblocking(sock);
    // bind
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    // addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_addr.s_addr = inet_addr(ip_addr);
    addr.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("bind()");
        return 1;
    }
    // default backlog is 128 => update by adding net.core.somaxconn=1024 to /etc/sysctl.conf
    if (listen(sock, SOMAXCONN) < 0) {
        perror("listen()");
        return 1;
    }
    // create the epoll socket
    int epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("epoll_create1()");
        return 1;
    }
    // mark the server socket for reading, and become edge-triggered
    struct epoll_event event;
    memset(&event, 0, sizeof(event));
    event.data.fd = sock;
    event.events = EPOLLIN | EPOLLET;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sock, &event) == -1) {
        perror("epoll_ctl()");
        return 1;
    }

    struct epoll_event *events = calloc(MAXEVENTS, sizeof(event));
    for (;;) {
        int nevents = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
        if (nevents == -1) {
            perror("epoll_wait()");
            return 1;
        }
        int i;
        for (i = 0; i < nevents; i++) {
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN))) {
                fprintf(stderr, "epoll event %d (%s)\n", events[i].events, strerror(events[i].events));
                close(events[i].data.fd);
                continue;
            } else if (events[i].data.fd == sock) {
                // server socket; call accept as many times as we can
                for (;;) {
                    struct sockaddr in_addr;
                    socklen_t in_addr_len = sizeof(in_addr);
                    int client = accept(sock, &in_addr, &in_addr_len);
                    if (client == -1) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK) {
                            // we processed all of the connections
                            break;
                        } else {
                            perror("accept()");
                            return 1;
                        }
                    } else {
                        printf(KGRN "%s: \tAccept new connection fd (%d). Connections "
                                    "count (%d)\n" RESET,
                               __func__, client, ++global_count);
                        // sockopt(client);
                        // set_nonblocking(client);
                        // set_keepalive(client);
                        event.data.fd = client;
                        event.events = EPOLLIN | EPOLLET;
                        if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, client, &event) == -1) {
                            perror("epoll_ctl()");
                            return 1;
                        }
                    }
                }
            } else {
                for (;;) {
                    char buf[1024];
                    bzero(buf, sizeof(buf));
                    int recvflags = 0;
                    ssize_t nbytes = recvfrom(events[i].data.fd, buf, sizeof(buf), recvflags, NULL, NULL);
                    if (nbytes == -1) {
                        // perror("recvfrom()");
                        if (errno == EAGAIN || errno == EWOULDBLOCK) {
                            break;
                        }
                    } else if (0 == nbytes) {
                        int fd = events[i].data.fd;
                        fd_list = rm_fd(fd_list, fd);
                        printf(KGRN "%s: \tRemove connection fd (%d) from list. "
                                    "Connections count (%d).\n" RESET,
                               __func__, fd, --global_count);
                        print_fdl(fd_list);
                        break;
                    } else if (1 == nbytes) {
                        printf("Consumer subscription topic: %s\n", buf);
                        // Insert element in list
                        fdl_t e;
                        e.fd = events[i].data.fd;
                        set_nonblocking(e.fd);
                        // sockopt(e.fd, 275);
                        e.topic = buf[0];
                        fd_list = add_fd(fd_list, e);
                        print_fdl(fd_list);
                    } else if (1 < nbytes) {
                        // printf("buf=%s\n", buf);
                        fdl_t *tmp = fd_list;
                        while (tmp) {
                            if (tmp->topic == buf[0]) {
                                // Send buffer to associated socket
                                int flags = O_NONBLOCK;
                                if (sendto(tmp->fd, buf, strlen(buf), flags, NULL, 0) < 0) {
                                    perror("sendto()");
                                }
                                // usleep(100); // Simulate delay
                            }
                            tmp = tmp->next;
                        }
                    }
                }
            }
        }
    }
}
