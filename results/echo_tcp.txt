./bin/echo_tcp_client 127.0.0.1 65111 1000000 0

length errors: 0
avg interval: 148.71
interval (usec):
usec_0_125: 41.97%
usec_125_250: 52.69%
usec_250_500: 4.54%
usec_500_750: 0.44%
usec_750_1000: 0.16%
usec_1000: 0.20%
TOTAL: 1000000
------------------------
length errors: 0
avg latency: 53.05
latency (usec):
usec_0_125: 96.25%
usec_125_250: 3.05%
usec_250_500: 0.60%
usec_500_750: 0.05%
usec_750_1000: 0.02%
usec_1000: 0.03%
TOTAL: 1000000


Sur nenuphar 1.70GHz
taskset -c 1 ./bin/echo_tcp_server 65111

length errors: 0
avg interval: 78.54
interval (usec):
usec_0_125: 99.99%
usec_125_250: 0.01%
usec_250_500: 0.00%
usec_500_750: 0.00%
usec_750_1000: 0.00%
usec_1000: 0.00%
TOTAL: 1000000
------------------------
length errors: 0
avg latency: 23.50
latency (usec):
usec_0_125: 100.00%
usec_125_250: 0.00%
usec_250_500: 0.00%
usec_500_750: 0.00%
usec_750_1000: 0.00%
usec_1000: 0.00%
TOTAL: 1000000

