#!/bin/bash

ip=$1
port=$2
n=$3

for i in `seq 1 $n`
do
   core=$(($i+1))
   docker_cmd="VMA_SPEC=latency VMA_THREAD_MODE=3 VMA_MEM_ALLOC_TYPE=0 LD_PRELOAD=libvma.so /sdl/bin/sdl_udp_consumer "$ip" "$port" MICRO_SERVICES_INFO 256"
   cmd=( docker run --cpuset-cpus=$core --network=host -t --ulimit memlock=-1 --device=/dev/infiniband/uverbs0 --device=/dev/infiniband/uverbs1 --device=/dev/infiniband/rdma_cm sdl_docker sh -c "$docker_cmd" )
   "${cmd[@]}" &
done

