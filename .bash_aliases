alias indent='indent -linux -l120 -i4 -nut'
alias clang-format="clang-format -style=\"{BasedOnStyle: llvm, ColumnLimit: 240, IndentWidth: 4}\" "
