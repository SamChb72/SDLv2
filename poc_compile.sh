########################################################
#       Copyright (C) 2018 GreenFLOWS                  #
#                                                      #
# This file is part of GreenFLOWS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOWS      #
#                                                      #
#             samy.chbinou@greenflows.fr               #
########################################################

#!/bin/bash

CCOPTS="-O3 -Wall -Werror -fPIC"
#CCOPTS="-g -Wall -Werror"

bindir=bin
libdir=lib

# Clean objects, binaries, ident code
#./clean.sh
clang-format -style="{BasedOnStyle: llvm, ColumnLimit: 240, IndentWidth: 4}" -i *.h *.c
rm $bindir/poc* $libdir/libpoc*

#
# Compile server, producer and consumer
#
mkdir -p $bindir
mkdir -p $libdir

echo "BUILD POC LIBRARY"
gcc $CCOPTS -o $bindir/poc_sdl_client.o -c poc_sdl_client.c -lm -lpthread
gcc $CCOPTS -shared -o $libdir/libpoc_sdl_client.so $bindir/poc_sdl_client.o -lm -lpthread

echo "BUILD POC EXEMPLE"
gcc $CCOPTS -o $bindir/poc_sdl_producer_exemple poc_sdl_producer_exemple.c -L$libdir -lpoc_sdl_client -lm -lrt
gcc $CCOPTS -o $bindir/poc_sdl_consumer_exemple poc_sdl_consumer_exemple.c -L$libdir -lpoc_sdl_client -lm -lrt

echo "BUILD POC UDP BROCKER & CLIENTS"
gcc $CCOPTS -o $bindir/poc_sdl_udp_brocker poc_sdl_udp_brocker.c -lm && gcc $CCOPTS -o $bindir/poc_sdl_udp_producer poc_sdl_udp_producer.c -lm && gcc $CCOPTS -o $bindir/poc_sdl_udp_consumer poc_sdl_udp_consumer.c -lm

echo "STRIPPING BINARIES"
cd $bindir && strip * && cd ..

echo "COPY stript for graph display"
sudo cp sdl.chart.sh /usr/libexec/netdata/charts.d/sdl.chart.sh
sudo systemctl restart netdata

echo "DONE"
