#!/bin/bash

ip=$1
port=$2
n=$3

for i in `seq 1 $n`
do
   echo "Start with core 2"
   core=$(($i+1))
   cmd="taskset -c $core ./bin/sdl_udp_consumer $ip $port MICRO_SERVICES_INFO 256"
   echo $cmd
   $cmd &
done

