#include "poc_sdl_client.h"
#include <signal.h>

#define MAX_MSG 50000000
#define interval_init 50000
int interval_set_nanosec = interval_init;
int new_interval = interval_init;
timer_t timer_set;
int producer_id;
struct timeval t0;

void write_to_charts(int timer_interval, float real_interval) {
    char filename[64];
    bzero(&filename, sizeof(filename));
    sprintf(filename, "/tmp/producer_%d", 0);
    FILE *f = fopen(filename, "w");
    if (f == NULL) {
        exit(1);
    }
    if (real_interval <= 0)
        fprintf(f, "%.2f -.-\n", (float)timer_interval / 1000);
    else
        fprintf(f, "%.2f %.2f\n", (float)timer_interval / 1000, (float)real_interval);
    fclose(f);
}

void start_timer_set(void) {
    struct itimerspec value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = interval_set_nanosec;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = interval_set_nanosec;
    timer_create(CLOCK_REALTIME, NULL, &timer_set);
    timer_settime(timer_set, 0, &value, NULL);
    write_to_charts(interval_set_nanosec, -1);
}

void stop_timer_set(void) {
    struct itimerspec value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = 0;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = 0;
    timer_settime(timer_set, 0, &value, NULL);
}

int check_interval_update(int nbmsg) {
    if (nbmsg < 5000000)
        return nbmsg;

    // else change interval
    new_interval -= 5000;

    if (!new_interval)
        new_interval = interval_init;

    struct itimerspec value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_nsec = new_interval;
    value.it_interval.tv_sec = 0;
    value.it_interval.tv_nsec = new_interval;
    timer_settime(timer_set, 0, &value, NULL);

    printf("set_row() timer call rate = %d μs\n", new_interval / 1000);

    write_to_charts(new_interval, -1);

    return 0;
}

void timer_set_callback(int sig) {
    struct timeval t;
    char ms[ROW_MAXLENGTH];
    static int nbmsg = 0;

    static unsigned long long count = 0;
    if (count * new_interval >= 1e9) {
        struct timeval t1;
        gettimeofday(&t1, NULL);
        int duration = (t1.tv_sec - t0.tv_sec) * (int)1e6 + (t1.tv_usec - t0.tv_usec);
        gettimeofday(&t0, NULL);
        if (duration < 0)
            duration = 0;
        printf("3x set_row() real call rate = %.2f μs\n", (float)duration / (float)count);
        write_to_charts(new_interval, (float)duration / (float)count);
        count = 0;
    } else
        count++;

    bzero(&ms, sizeof(ms));
    gettimeofday(&t, NULL);
    char *data1 = "a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,STARTING,FEU,";
    sprintf(ms, "%d:%s%ld", (int)strlen(data1) + 17, data1, (t.tv_sec) * (int)1e6 + (t.tv_usec));
    sdl_producer_set_row(producer_id, "MICRO_SERVICES_INFO", rand() % ROW_NB, ms);
    nbmsg = check_interval_update(++nbmsg);

    bzero(&ms, sizeof(ms));
    gettimeofday(&t, NULL);
    char *data2 = "a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,RUNNING,FEU,";
    sprintf(ms, "%d:%s%ld", (int)strlen(data2) + 17, data2, (t.tv_sec) * (int)1e6 + (t.tv_usec));
    sdl_producer_set_row(producer_id, "MICRO_SERVICES_INFO", rand() % ROW_NB, ms);
    nbmsg = check_interval_update(++nbmsg);

    bzero(&ms, sizeof(ms));
    gettimeofday(&t, NULL);
    char *data3 = "a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,STOPPING,FEU,";
    sprintf(ms, "%d:%s%ld", (int)strlen(data3) + 17, data3, (t.tv_sec) * (int)1e6 + (t.tv_usec));
    sdl_producer_set_row(producer_id, "MICRO_SERVICES_INFO", rand() % ROW_NB, ms);
    nbmsg = check_interval_update(++nbmsg);
}
/*

"microServiceId":"a13e3c5def37342a"
{"microServiceOrchProvider":"Swarm","microServiceName":"BBSC","sourcePortNo":51345," chainingPortRange":1024,"sourceIpAddress":"172.1.0.15","status":"RUNNING","deployedSer ver":"FEU","startDate":1479829775}
*/

int main(int argc, char **argv) {
    srand(time(NULL));
    sdl_interface_init();
    producer_id = sdl_producer_init(argv[1], 65111);

    (void)signal(SIGALRM, timer_set_callback);
    start_timer_set();

    while (1)
        ;

    // Uneusefull. just here to avoid compile error
    int s = sizeof(mutexes.mutex_consume);
    // s = sizeof(mutexes.cond_consume);
    // s = sizeof(mutexes.cond_display);
    s++;

    return 0;
}
