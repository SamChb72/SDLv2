
#include "sdl.h"

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char **argv) {
    int sockfd, portno;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    int nbmsg, intvl;
    counters_t counters;
    bzero(&counters, sizeof(counters));
    counters_t intervals;
    bzero(&intervals, sizeof(intervals));
    char host[32];
    bzero(&host, sizeof(host));
    gethostname(host, sizeof(host));

    /* check command line arguments */
    if (argc != 5) {
        fprintf(stderr, "usage: %s <hostname> <port> <number of msg> <us interval between msg>\n", argv[0]);
        exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);
    nbmsg = atoi(argv[3]);
    intvl = atoi(argv[4]);

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
        error("ERROR connecting");
    int count = 0;
    for (count = 0; count < nbmsg; count++) {
        struct timeval t0;
        char buf[BUFSIZE];
        bzero(buf, BUFSIZE);
        gettimeofday(&t0, NULL);
        sprintf(buf, "%c:%ld:%s", '1', (t0.tv_sec) * (int)1e6 + (t0.tv_usec), PAYLOAD_256);
        /* send the message line to the server */
        if (0 > write(sockfd, buf, strlen(buf))) {
            error("ERROR writing to socket");
        }
        /* Read the echo */
        bzero(buf, BUFSIZE);
        if (0 > read(sockfd, buf, BUFSIZE)) {
            perror("read()");
        }
        print_itvl_stats(&intervals, t0, 10000);
        print_rcv_stats(host, &counters, 256, buf, 10000);
        usleep(intvl);
    }
    close(sockfd);
    return 0;
}
