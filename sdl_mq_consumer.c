#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "sdl.h"

int main(int argc, char **argv) {
    mqd_t down;

    /* open the DownLink queue */
    down = mq_open(QUEUE_DOWN, O_RDONLY);
    CHECK((mqd_t)-1 != down);

    //    do {
    //    } while (strncmp(buffer, MSG_STOP, strlen(MSG_STOP)));

    counters_t counters;
    bzero(&counters, sizeof(counters_t));

    counters.total = 0;
    for (;;) {
        char rcvbuf[BUFSIZE];
        bzero(rcvbuf, BUFSIZE);
        /* receive the echo message */
        ssize_t bytes_read;
        bytes_read = mq_receive(down, rcvbuf, MAX_SIZE, NULL);
        CHECK(bytes_read >= 0);
        if (!bytes_read)
            break;
        rcvbuf[bytes_read] = '\0';
        print_rcv_stats(hostname, &counters, rcvbuf);
    }

    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(down));

    return 0;
}
