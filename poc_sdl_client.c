//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#              samy.chbinou@greenflows.fr              #
//########################################################

#define _GNU_SOURCE
#include <sched.h>

#include "poc_sdl_client.h"

/* Display producer list */
void print_producer_list() {
    producer_t *l = producer_list;
    while (l) {
        printf("%d:%lu\n", l->id, l->addr);
        l = l->next;
    }
}

/* Search a producer in list and return associated address */
int get_brockeraddr(int id, struct sockaddr_in *addr) {
    while (producer_list) {
        if (producer_list->id == id) {
            memcpy(addr, &producer_list->csockaddr_in, sizeof(struct sockaddr_in));
            return EXIT_SUCCESS;
        }
        producer_list = producer_list->next;
    }
    return EXIT_FAILURE;
}

/* Add producer to producer list */
producer_t *add_producer(producer_t v) {
    producer_t *new = (producer_t *)calloc(1, sizeof(producer_t));

    new->id = v.id;
    new->addr = v.addr;
    memcpy(&new->csockaddr_in, &v.csockaddr_in, sizeof(v.csockaddr_in));
    new->next = producer_list;

    return new;
}

void sdl_interface_init() { producer_list = NULL; }

int sdl_connect_to_brocker(char *ip, int port, struct sockaddr_in *brockeraddr) {
    struct hostent *brocker;
    char *hostname;

    hostname = ip;

    /* gethostbyname: get the brocker's DNS entry */
    brocker = gethostbyname(hostname);
    if (brocker == NULL) {
        perror("gethostbyname()");
        exit(EXIT_FAILURE);
    }

    /* build the brocker's Internet address */
    brockeraddr->sin_family = AF_INET;
    bcopy((char *)brocker->h_addr, (char *)&brockeraddr->sin_addr.s_addr, brocker->h_length);
    brockeraddr->sin_port = htons(port);

    /* socset: create the socket */
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        perror("socket()");

    /* connect: create a connection with the brocker */
    if (connect(sockfd, (struct sockaddr *)brockeraddr, sizeof(struct sockaddr)) < 0) {
        perror("connect() to brocker");
        close(sockfd);
        return EXIT_FAILURE;
    }
    printf("Connected\n");
    return sockfd;
}

/* IN: ip address, port number  */
/* OUT: producerId		*/
int sdl_producer_init(char *ip, int port) {
    struct sockaddr_in brockeraddr;
    bzero((char *)&brockeraddr, sizeof(brockeraddr));
    int sockfd = sdl_connect_to_brocker(ip, port, &brockeraddr);
    if (-1 == sockfd)
        return EXIT_FAILURE;

    producer_t p;
    bzero(&p, sizeof(producer_t));
    p.id = sockfd;
    p.addr = brockeraddr.sin_addr.s_addr;
    memcpy(&p.csockaddr_in, &brockeraddr, sizeof(brockeraddr));
    p.count = 1;
    producer_list = add_producer(p);
    print_producer_list();
    return p.id;
}

int sdl_producer_set_row(int producer_id, char *topic, int row_id, char *row) {
    char buf[ROW_MAXLENGTH];
    struct timeval t0;
    bzero(&buf, sizeof(buf));
    gettimeofday(&t0, NULL);
    unsigned char topicId = topic_id(topic);
    sprintf(buf, "%c:%ld:%d:%s", topicId, (t0.tv_sec) * (int)1e6 + (t0.tv_usec), row_id, row);

    // printf("SEND %s\n", buf);
    producer_t *l = producer_list;
    while (l) {
        if (l->id == producer_id) {
            // Send buffer to associated consumer address
            int flags = 0;
            if (sendto(producer_id, buf, strlen(buf), flags, (struct sockaddr *)&l->csockaddr_in, sizeof(l->csockaddr_in)) < 0) {
                perror("sendto()");
            }
        }
        l = l->next;
    }
    return EXIT_SUCCESS;
}

void *thread_consume(void *args) {
    consume_t *ct = args;
    struct timeval t1;
    char buf[ROW_MAXLENGTH];

    // Thread affinity
    cpu_set_t cpuset;
    char *env_cpu = getenv("CONSUMER_CPU");
    int cpu;
    if (env_cpu)
        cpu = atoi(env_cpu);
    else
        cpu = 3;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);
    sched_setaffinity(0, sizeof(cpuset), &cpuset);

    // unsigned int msgnb = 0;
    for (;;) {
        bzero(buf, ROW_MAXLENGTH);
        int recvflags = O_NONBLOCK; // | MSG_DONTWAIT;
        int nbytes = recvfrom(ct->sock_id, buf, sizeof(buf), recvflags, NULL, NULL);
        if (nbytes < 0) {
            perror("recvfrom()");
            ;
        }
        gettimeofday(&t1, NULL);

        pthread_mutex_lock(&mutexes.mutex_consume);
        gettimeofday(&ct->stats.ts, NULL);
        //        pthread_cond_wait(&mutexes.cond_consume, &mutexes.mutex_consume);

        // warmup caches
        /*
        if (++msgnb < 100000)
            continue;
        else
            msgnb = 100000;
        */

        char c = 0;
        unsigned long ts = 0;
        int row_id = 0;
        int id_l = 0;
        int size = 0;
        int size_l = 0;
        char pload[ROW_MAXLENGTH];

        sscanf(buf, "%c:%lu:%d:%d:%s", &c, &ts, &row_id, &size, pload);

        if (row_id < 10)
            id_l = 1;
        else if (row_id < 100)
            id_l = 2;
        else if (row_id < 1000)
            id_l = 3;
        else if (row_id < 10000)
            id_l = 4;

        if (size < 10)
            size_l = 1;
        else if (size < 100)
            size_l = 2;
        else if (size < 1000)
            size_l = 3;

        /*%c:%ld:%d:%s"
        5:1533983118545584:12:79:a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,RUNNING,FEU,1533983118545583
        5:1533983118545644:3:79:a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,RUNNING,FEU,1533983118545644
        5:1533983118545642:4:80:a13e3c5def37342a,Swarm,BBSC,51345,1024,172.1.0.15,STARTING,FEU,1533983118545641

        1+1 + 16+1 +row_l+1 + rowbuf_l+1 + size -1
        */
        if (1 + 1 + 16 + 1 + id_l + 1 + size_l + 1 + size - 1 != nbytes) {
            printf("%d != %d\n", 1 + 1 + 16 + 1 + id_l + 1 + size_l + 1 + size - 1, nbytes); // (size_l - size_l)
            printf("%ld %d %d %d %d %s\n", ts, row_id, id_l, size, size_l, buf);
            ct->stats.size_errors++;
            pthread_mutex_unlock(&mutexes.mutex_consume);
            continue;
        }
        memcpy((ct->table + row_id * ROW_MAXLENGTH), buf, nbytes);

        ct->stats.total++;
        ct->stats.sum_size += nbytes;
        ct->stats.latency = (ct->stats.ts.tv_sec) * (int)1e6 + (ct->stats.ts.tv_usec) - ts;
        ct->stats.sum += ct->stats.latency;

        int square = ct->stats.latency * ct->stats.latency;
        ct->stats.sum_1k += ct->stats.latency;
        ct->stats.sum_1k -= ct->stats.latency_1k[ct->stats.total % 1000];
        ct->stats.sum2_1k += square;
        ct->stats.sum2_1k -= (ct->stats.latency_1k[ct->stats.total % 1000] * ct->stats.latency_1k[ct->stats.total % 1000]);
        ct->stats.latency_1k[ct->stats.total % 1000] = ct->stats.latency;

        ct->stats.sum_10k += ct->stats.latency;
        ct->stats.sum_10k -= ct->stats.latency_10k[ct->stats.total % 10000];
        ct->stats.sum2_10k += square;
        ct->stats.sum2_10k -= (ct->stats.latency_10k[ct->stats.total % 10000] * ct->stats.latency_10k[ct->stats.total % 10000]);
        ct->stats.latency_10k[ct->stats.total % 10000] = ct->stats.latency;

        ct->stats.sum_100k += ct->stats.latency;
        ct->stats.sum_100k -= ct->stats.latency_100k[ct->stats.total % 100000];
        ct->stats.sum2_100k += square;
        ct->stats.sum2_100k -= (ct->stats.latency_100k[ct->stats.total % 100000] * ct->stats.latency_100k[ct->stats.total % 100000]);
        ct->stats.latency_100k[ct->stats.total % 100000] = ct->stats.latency;

        // ct->stats.avg = (float)(ct->stats.sum) / (float)(++ct->stats.total);
        ct->stats.sum2 += ct->stats.latency * ct->stats.latency;
        // ct->stats.sig2 = (float)ct->stats.sum2 / (float)(ct->stats.total) - (float)(ct->stats.avg * ct->stats.avg);
        // ct->stats.bw = 8.0 * (float)ct->stats.size / (float)ct->stats.sum;

        if (ct->stats.min > ct->stats.latency) {
            ct->stats.min = ct->stats.latency;
        }
        if (ct->stats.max < ct->stats.latency) {
            ct->stats.max = ct->stats.latency;
        }

        if (ct->stats.latency < 15)
            ct->stats.cnt._0_15++;
        else if (ct->stats.latency < 25)
            ct->stats.cnt._15_25++;
        else if (ct->stats.latency < 50)
            ct->stats.cnt._25_50++;
        else if (ct->stats.latency < 75)
            ct->stats.cnt._50_75++;
        else if (ct->stats.latency < 100)
            ct->stats.cnt._75_100++;
        else if (ct->stats.latency < 500)
            ct->stats.cnt._100_500++;
        else if (ct->stats.latency < 1000)
            ct->stats.cnt._500_1000++;
        else
            ct->stats.cnt._1000p++;

        //        pthread_cond_signal(&mutexes.cond_display);
        pthread_mutex_unlock(&mutexes.mutex_consume);
    }
    free(ct->table);
    free(ct);
    pthread_exit(NULL);
    return NULL;
}

consume_t *sdl_consumer_start(char *ip, int port, char *topic) {
    struct sockaddr_in brockeraddr;
    bzero((char *)&brockeraddr, sizeof(brockeraddr));
    int sockfd = sdl_connect_to_brocker(ip, port, &brockeraddr);
    if (-1 == sockfd)
        return NULL;

    /* Subscription */
    char tpid = topic_id(topic);
    int nbytes = sendto(sockfd, &tpid, 1, 0, (struct sockaddr *)&brockeraddr, sizeof(brockeraddr));
    if (nbytes < 0) {
        printf("Subscription failed. ");
        perror("sendto()");
        close(sockfd);
        return NULL;
    }

    pthread_t pth;
    consume_t *args = calloc(1, sizeof *args);
    args->id = 0;
    args->table = (char *)calloc(ROW_NB, ROW_MAXLENGTH);
    args->sock_id = sockfd;
    args->stats.min = 999999;
    if (pthread_create(&pth, NULL, thread_consume, args)) {
        free(args);
    }
    args->thread_id = pth;
    return args;
}

char *sdl_get_row(char *table, int row_id) {
    if (!table)
        return "";
    else
        return (char *)(table + row_id * ROW_MAXLENGTH);
}

void sdl_consumer_stop(consume_t *args) {
    pthread_cancel(args->thread_id);
    free(args->table);
    free(args);
    // send unsubcription to brocker
}
