########################################################
#       Copyright (C) 2018 GreenFLOWS                  #
#                                                      #
# This file is part of GreenFLOWS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOWS      #
#                                                      #
#             samy.chbinou@greenflows.fr               #
########################################################



prjdir=`pwd`

# Clean and Compile
./compile.sh


#
# Create debian package
#
cd $prjdir
lastbuild=`cat lastbuild`
currentbuild=$( expr "$lastbuild" + 1 )
echo $currentbuild > lastbuild
pkgname=SDLv2.${currentbuild}_i386.deb
echo "Packaging $pkgname"

mkdir -p pkg_build_dir/SDLv2/DEBIAN
sed "s/currentbuild/$currentbuild/" control_tpl > pkg_build_dir/SDLv2/DEBIAN/control

mkdir -p pkg_build_dir/SDLv2/usr/bin
cp bin/* pkg_build_dir/SDLv2/usr/bin/.

mkdir -p arch
cd pkg_build_dir
dpkg --build SDLv2 ../arch/$pkgname &&
cd ..

#
# Build docker image containing the debian package
#
rm -rf docker
mkdir -p docker
cp arch/$pkgname docker/$pkgname
sed "s/pkgname/$pkgname/" dockerfile_tpl > docker/dockerfile
cd docker
docker build -t sdlv2.${currentbuild} .
docker run --network=host -it sdlv2.${currentbuild}

