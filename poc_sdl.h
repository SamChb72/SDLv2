/*
########################################################
#       Copyright (C) 2018 GreenFLOWS                  #
#                                                      #
# This file is part of GreenFLOWS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOWS      #
#                                                      #
#            samy.chbinou@greenflows.fr                #
########################################################
*/

/* Shared Data Layer for Microservices communications */

#ifndef sdl_h__
#define sdl_h__

#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "poc_sdl_types.h"

#define true 1
#define false 0

#define KGRN "\x1B[32m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define RESET "\x1B[0m"

#define TOPICNAME_MAXLEN 32

#define QUEUE_UP "/sdl_up"
#define QUEUE_DOWN "/sdl_down"
#define MAX_SIZE 1024
#define BUFSIZE 512

#define PAYLOAD_32 "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

#define PAYLOAD_64 "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

#define PAYLOAD_128 "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

#define PAYLOAD_256                                                                                                                                                                                                                            \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" \
    "AAAAAAAAAAAAAAAAAAAAAAAA"

#define PAYLOAD_512                                                                                                                                                                                                                            \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

#define CHECK(x)                                                                                                                                                                                                                               \
    do {                                                                                                                                                                                                                                       \
        if (!(x)) {                                                                                                                                                                                                                            \
            fprintf(stderr, "%s:%d: ", __func__, __LINE__);                                                                                                                                                                                    \
            perror(#x);                                                                                                                                                                                                                        \
            exit(-1);                                                                                                                                                                                                                          \
        }                                                                                                                                                                                                                                      \
    } while (0)

/* List of consumers socket descriptors TODO: Blocks of topicnames */
/*
in TCP mode use fd is descriptor of accepted socket
in UDP mode use fd represents s_addr ipv4 address
struct in_addr {
    unsigned long s_addr;          // load with inet_pton()
};
*/
typedef struct fdl_t fdl_t;
struct fdl_t {
    int fd;             // tcp
    unsigned long addr; // udp
    unsigned int count;
    struct sockaddr_in csockaddr_in;
    unsigned char topic;
    fdl_t *next;
};

int global_count = 0;

typedef struct counters_t counters_t;
struct counters_t {
    uint32_t total;
    struct timeval t;
    float avg;
    long int sum;
    long int sum2;
    float sig2;
    float min;
    float max;
    uint32_t lengtherror;
    uint32_t distr[7];
};

void printcnt(char *name, counters_t *cnt);
void printfile(char *name, counters_t *cnt);

void print_rcv_stats(char *hostname, counters_t *cnt, unsigned int msglen, char *rcvbuf, int every) {
    struct timeval t1;
    gettimeofday(&t1, NULL);
    unsigned int len = strlen(rcvbuf);
    char str[64];
    bzero(&str, sizeof(str));
    sprintf(str, "%s e2e latency", hostname);
    static unsigned char warmup = 1;

    if (19 + 4 == len) {
        printcnt(str, cnt);
    } else if (19 + msglen != len) {
        // printf("msglen: %u, len: %u, buf: %s\n", msglen, len, rcvbuf);
        cnt->lengtherror++;
    }
    cnt->total++;
    // bypass 1000 first messages: cache warmup
    if (warmup && cnt->total < 1000)
        return;
    if (warmup && 1000 == cnt->total) {
        warmup = 0;
        cnt->total = 1;
    }
    char *t0 = NULL;
    t0 = strtok(rcvbuf, ":");
    t0 = strtok(NULL, ":");
    if (NULL != t0) {
        long int lat = (t1.tv_sec) * (int)1e6 + (t1.tv_usec) - atol(t0);
        int n = cnt->total;
        if (cnt->min > lat) {
            cnt->min = lat;
        }
        if (cnt->max < lat) {
            cnt->max = lat;
        }
        cnt->sum += lat;
        cnt->avg = (float)(cnt->sum) / (float)n;
        cnt->sum2 += lat * lat;
        cnt->sig2 = (float)cnt->sum2 / (float)n - (cnt->avg * cnt->avg);

        /*
        μ - σ
        μ - 0.5σ
        μ + 0.5σ
        μ + σ
        μ + 2σ
        */

        float mean = cnt->avg;
        float sigma = sqrt(cnt->sig2);

        if (lat < mean - sigma)
            cnt->distr[0]++;
        if (lat < mean - 0.5 * sigma)
            cnt->distr[1]++;
        if (lat < mean)
            cnt->distr[2]++;
        if (lat < mean + 0.5 * sigma)
            cnt->distr[3]++;
        if (lat < mean + sigma)
            cnt->distr[4]++;
        if (lat < mean + 2 * sigma)
            cnt->distr[5]++;
        else
            cnt->distr[6]++;

        // Print stats every n messages
        if (0 == (cnt->total) % every) {
            printcnt(str, cnt);
            // printfile("latency", cnt);
        }
    }
}

void printfile(char *name, counters_t *cnt) {
    FILE *fp;
    char str[128];
    time_t t = time(NULL);
    struct tm *pTime = localtime(&t);
    char buffer[25];
    bzero(buffer, sizeof(buffer));
    strftime(buffer, 25, "%H:%M:%S", pTime);

    bzero(&str, sizeof(str));
    fp = fopen("/data/sdl_udp_consumer.csv", "a");
    /*
        sprintf(str, "%s,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d\n", buffer, cnt->lengtherror, cnt->avg, (double)100 * cnt->usec_0_125 / cnt->total, (double)100 * cnt->usec_125_250 / cnt->total, (double)100 * cnt->usec_250_500 /
       cnt->total,
                (double)100 * cnt->usec_500_750 / cnt->total, (double)100 * cnt->usec_750_1000 / cnt->total, (double)100 * cnt->usec_1000 / cnt->total, cnt->total);
    */
    fwrite(str, 1, sizeof(str), fp);
    fclose(fp);
}

void printcnt(char *name, counters_t *cnt) {
    time_t t = time(NULL);
    struct tm *pTime = localtime(&t);
    char buffer[25];
    bzero(buffer, sizeof(buffer));
    strftime(buffer, 25, "%H:%M:%S", pTime);
    /* Add verbosity option */
    float mean = cnt->avg;
    float sigma = sqrt(cnt->sig2);

    printf("%12s%25s%15s%20s%20s%20s%19s%19s\n%12s%25s%15d%20d%18.3f%18.3f%18.3f%18.3f\nRepartition\n<%.3f %.3f%%    <%.3f %.3f%%    <%.3f %.3f%%    <%.3f %.3f%%    <%.3f %.3f%%    <%.3f %.3f%%    >%.3f %.3f%%\n", "TimeStamp",
           "hostname & measure", "number of MSG", "MSG length errors", "mean μ [μs]", "variance σ [μs]", "peak min [μs]", "peak max [μs]", buffer, name, cnt->total, cnt->lengtherror, cnt->avg, sqrt(cnt->sig2), cnt->min, cnt->max,
           mean - sigma, 100.0 * cnt->distr[0] / cnt->total, mean - 0.5 * sigma, 100.0 * cnt->distr[1] / cnt->total, mean, 100.0 * cnt->distr[2] / cnt->total, mean + 0.5 * sigma, 100.0 * cnt->distr[3] / cnt->total, mean + sigma,
           100.0 * cnt->distr[4] / cnt->total, mean + 2 * sigma, 100.0 * cnt->distr[5] / cnt->total, mean + 2 * sigma, 100.0 * cnt->distr[6] / cnt->total);
}

void print_itvl_stats(counters_t *intervals, struct timeval t, int every) {
    long int itvl = 0;

    if (0 == intervals->total) {
        memcpy(&intervals->t, &t, sizeof(struct timeval));
        intervals->total++;
    } else {
        int n = intervals->total++;
        itvl = ((t.tv_sec) * (int)1e6 + (t.tv_usec)) - ((intervals->t.tv_sec) * (int)1e6 + (intervals->t.tv_usec));
        memcpy(&intervals->t, &t, sizeof(struct timeval));

        if (intervals->min > itvl) {
            intervals->min = itvl;
        }
        if (intervals->max < itvl) {
            intervals->max = itvl;
        }
        intervals->sum += itvl;
        intervals->avg = (float)(intervals->sum) / (float)n;
        intervals->sum2 += itvl * itvl;
        intervals->sig2 = (float)intervals->sum2 / (float)n - (intervals->avg * intervals->avg);

        /*
        μ - σ
        μ - 0.5σ
        μ + 0.5σ
        μ + σ
        μ + 2σ
        */
        float mean = intervals->avg;
        float sigma = sqrt(intervals->sig2);

        if (itvl < mean - sigma)
            intervals->distr[0]++;
        if (itvl < mean - 0.5 * sigma)
            intervals->distr[1]++;
        if (itvl < mean)
            intervals->distr[2]++;
        if (itvl < mean + 0.5 * sigma)
            intervals->distr[3]++;
        if (itvl < mean + sigma)
            intervals->distr[4]++;
        if (itvl < mean + 2 * sigma)
            intervals->distr[5]++;
        else
            intervals->distr[6]++;
    }
    // Print stats every n messages
    if (0 == (intervals->total) % every) {
        printcnt("interval", intervals);
    }
}

void sockopt(int sockfd, size_t t1) {
    /* Socket properties */
    socklen_t slen;
    int len;

    int one = 1;
    setsockopt(sockfd, SOL_TCP, TCP_NODELAY, &one, sizeof(one));
    setsockopt(sockfd, SOL_TCP, TCP_CORK, &one, sizeof(one));

    slen = sizeof(len);
    if (getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("receive buffer size = %d\n", len);

    if (getsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("send buffer size = %d\n", len);

    if (getsockopt(sockfd, SOL_SOCKET, SO_RCVLOWAT, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("rcv low watermark= %d\n", len);

    if (getsockopt(sockfd, SOL_SOCKET, SO_SNDLOWAT, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("snd low watermark = %d\n", len);

    size_t t2;
    t2 = sizeof(int);
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &t1, t2) < 0) {
        perror(": setsockopt");
    }
    if (getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("receive buffer size = %d\n", len);

    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &t1, t2) < 0) {
        perror(": setsockopt");
    }
    if (getsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &len, &slen) < 0) {
        perror(": getsockopt");
    }
    //    printf("send buffer size = %d\n", len);
}

void set_keepalive(int fd) {
    int enable = 1;    /* 1=KeepAlive On, 0=KeepAlive Off. */
    int idle = 60;     /* Number of idle seconds before sending a KA probe. */
    int interval = 10; /* How often in seconds to resend an unacked KA probe. */
    int count = 6;     /* How many times to resend a KA probe if previous probe was unacked. */

    if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable)) < 0) {
        perror("SO_KEEPALIVE");
    }
    if (enable) {
        if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(idle)) < 0)
            perror("TCP_KEEPIDLE");
        if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(interval)) < 0)
            perror("TCP_KEEPINTVL");
        if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT, &count, sizeof(count)) < 0)
            perror("TCP_KEEPCNT");
    }
}

#endif // sdl_h__
