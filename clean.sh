# Keep source code well formatted
clang-format -style="{BasedOnStyle: llvm, ColumnLimit: 240, IndentWidth: 4}" -i *.h *.c
# Clean
rm -rf bin pkg_build_dir docker
rm *~

