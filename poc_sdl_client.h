#ifndef _SDL_H_
#define _SDL_H_

#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include "poc_sdl_types.h"

#define ROW_NB 1000
#define ROW_MAXLENGTH 256

typedef struct {
    pthread_mutex_t mutex_consume;
    //    pthread_cond_t cond_consume;
    //    pthread_cond_t cond_display;
} mutexes_t;

static mutexes_t mutexes = {
    .mutex_consume = PTHREAD_MUTEX_INITIALIZER, //.cond_consume = PTHREAD_COND_INITIALIZER, .cond_display = PTHREAD_COND_INITIALIZER,
};

/* global interface */
void sdl_interface_init();

/* Producer interfaces */
/* initialize a producer returns producer_id */
int sdl_producer_init(char *ip, int port);
/* Create table row with row_id to topic */
int sdl_producer_set_row(int producer_id, char *topic, int row_id, char *row);

/* Consumer interfaces */
/* subscribe to brocker */
/* start consuming returns pointer to table */
consume_t *sdl_consumer_start(char *ip, int port, char *topic);
/* get table row */
char *sdl_get_row(char *table, int row_id);
/* stop consuming */
void sdl_consumer_stop(consume_t *args);

#endif
