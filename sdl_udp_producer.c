//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#              samy.chbinou@greenflows.fr              #
//########################################################

#include "sdl.h"
#include <netdb.h>

int create_connection(struct sockaddr_in serveraddr, unsigned char topicId, char *payload, int nmsg, int itvl) {
    char buf[BUFSIZE];
    counters_t intervals;
    bzero(&intervals, sizeof(intervals));
    intervals.min = 999999;

    /* socset: create the socket */
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        perror("socket()");

    // sockopt(sockfd, 19 + strlen(payload));

    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) {
        perror("connect() to brocker");
        close(sockfd);
        return EXIT_FAILURE;
    }
    printf("Connected\n");
    /* prepare payload and sendto brocker */
    int i;
    for (i = 0; i < nmsg + 1; i++) {
        struct timeval t0;
        memset(buf, '\0', BUFSIZE);
        gettimeofday(&t0, NULL);
        if (nmsg == i) {
            strcpy(payload, "STOP");
        }
        sprintf(buf, "%c:%ld:%s", topicId, (t0.tv_sec) * (int)1e6 + (t0.tv_usec), payload);
        // printf("Send %s\n", buf);
        int flags = 0; // | MSG_NOSIGNAL; // | MSG_DONTWAIT;
        int n = sendto(sockfd, buf, strlen(buf), flags, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
        // printf("n=%d\n", n);
        if (n < 0) {
            perror("sendto()");
            close(sockfd);
            return EXIT_FAILURE;
        }
        print_itvl_stats(&intervals, t0, 10000);
        usleep(itvl);
    }
    close(sockfd);
    return 0;
}

int main(int argc, char **argv) {
    int portno;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char *topic;
    int msglen;
    int nbmsg;
    int intvl;
    char pload[1024];

    /* check command line arguments */
    if (argc != 7) {
        fprintf(stderr, "usage: %s <hostname> <port> <topic> <pload size> <nb_messages> <usec_interval>\n", argv[0]);
        fprintf(stderr, "ex: %s 127.0.0.1 65111 MICRO_SERVICES_INFO 256 1000000 500\n", argv[0]);
        exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);
    topic = argv[3];
    msglen = atoi(argv[4]);
    nbmsg = atoi(argv[5]);
    intvl = atoi(argv[6]);

    /* check user parameters */

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        perror("gethostbyname()");
        exit(EXIT_FAILURE);
    }

    /* portno */
    if (portno <= 49151) {
        printf("This port number (%d) is reserved and cant be used.\nport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }
    if (portno >= 65535) {
        printf("This port number (%d) is too big.\nnport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }

    /* topic */
    if (strlen(topic) > 64) {
        printf("Please use a topic name less than 64 characters long\n");
        exit(EXIT_FAILURE);
    }

    /* msglen */
    memset(pload, '\0', sizeof(pload));
    if (16 != msglen && 32 != msglen && 64 != msglen && 128 != msglen && 256 != msglen && 512 != msglen) {
        printf("only payloads of 16, 32, 64, 128, 256 and 215 bytes length are supported\n");
        exit(EXIT_FAILURE);
    } else {
        int i = 0;
        for (i = 0; i < msglen; i++)
            pload[i] = 'A';
    }
    /* build the server's Internet address */
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    unsigned char topicId = topic_id(topic);
    int ret = create_connection(serveraddr, topicId, pload, nbmsg, intvl);
    while (EXIT_FAILURE == ret) {
        // Retry every second
        usleep(1000 * 1000);
        ret = create_connection(serveraddr, topicId, pload, nbmsg, intvl);
    }
}
