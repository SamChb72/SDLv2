#include <errno.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "sdl.h"

int main(int argc, char **argv) {
    mqd_t up, down;
    struct mq_attr attr;
    char buf[BUFSIZE + 1];

    /* initialize the queues attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_SIZE;
    attr.mq_curmsgs = 0;

    /* create the up message queue */
    up = mq_open(QUEUE_UP, O_CREAT | O_RDONLY, 0644, &attr);
    CHECK((mqd_t)-1 != up);

    /* create the down message queue */
    down = mq_open(QUEUE_DOWN, O_CREAT | O_WRONLY, 0644, &attr);
    CHECK((mqd_t)-1 != down);

    for (;;) {
        ssize_t bytes_read;
        char buf[BUFSIZE];
        bzero(buf, BUFSIZE);
        /* receive the message */
        bytes_read = mq_receive(up, buf, MAX_SIZE, NULL);
        CHECK(bytes_read >= 0);
        if (!bytes_read) {
            CHECK(0 <= mq_send(down, "", 0, 0));
            break;
        }
        buf[bytes_read] = '\0';
        // printf("Received: %s\n", buf);
        /* echo the message */
        CHECK(0 <= mq_send(down, buf, BUFSIZE, 0));
    }

    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(up));
    CHECK((mqd_t)-1 != mq_unlink(QUEUE_UP));
    CHECK((mqd_t)-1 != mq_close(down));
    CHECK((mqd_t)-1 != mq_unlink(QUEUE_DOWN));

    return 0;
}
