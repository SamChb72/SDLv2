#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "sdl.h"

#define PAYLOAD_256                                                                                                                                                                                                                            \
    "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" \
    "AAAAAAAAAAAAAAAAAAAAAAAA"

int main(int argc, char **argv) {
    mqd_t up;

    /* open the UpLink queue */
    up = mq_open(QUEUE_UP, O_WRONLY);
    CHECK((mqd_t)-1 != up);

    //    do {
    //    } while (strncmp(buffer, MSG_STOP, strlen(MSG_STOP)));

    int nbmsg = 1000000;
    int count = 0;
    counters_t counters;
    bzero(&counters, sizeof(counters_t));

    counters.total = 0;
    for (count = 0; count < nbmsg; count++) {
        struct timeval t0;
        char sndbuf[BUFSIZE], rcvbuf[BUFSIZE];

        bzero(sndbuf, BUFSIZE);
        bzero(rcvbuf, BUFSIZE);
        gettimeofday(&t0, NULL);
        sprintf(sndbuf, "%c:%ld:%s", '1', (t0.tv_sec) * (int)1e6 + (t0.tv_usec), PAYLOAD_256);
        /* send the message */
        CHECK(0 <= mq_send(up, sndbuf, BUFSIZE, 0));
    }
    mq_send(up, "", 0, 0);
    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(up));

    return 0;
}
