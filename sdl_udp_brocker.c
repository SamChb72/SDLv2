#include "sdl.h"

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(1);
}

/* Display list */
void print_fdl(fdl_t *l) {
    while (l) {
        printf("%c:%d:%lu\n", l->topic, l->fd, l->addr);
        l = l->next;
    }
}

/* Search an address in list and return number of associated clients*/
int search_fd(fdl_t *l, long int addr) {
    while (l) {
        if (l->addr == addr) {
            return l->count;
        }
        l = l->next;
    }
    return 0;
}

/* Remove an address from list */
fdl_t *rm_fd(fdl_t *l, int fd, long int addr) {
    if (!l)
        return NULL;
    if (!l->addr && l->fd == fd) {
        fdl_t *tmpnext;
        tmpnext = l->next;
        close(l->fd);
        free(l);
        return tmpnext;
    } else if (!l->fd && l->addr == addr) {
        fdl_t *tmpnext;
        tmpnext = l->next;
        l->addr = 0;
        l->count = 0;
        free(l);
        return tmpnext;
    }
    l->next = rm_fd(l->next, fd, addr);
    return l;
}

/* Add an address to list */
fdl_t *add_fd(fdl_t *l, fdl_t v) {
    fdl_t *new = (fdl_t *)malloc(sizeof(fdl_t));
    memset(new, 0, sizeof(fdl_t));

    new->fd = v.fd;
    new->addr = v.addr;
    memcpy(&new->csockaddr_in, &v.csockaddr_in, sizeof(v.csockaddr_in));
    new->topic = v.topic;
    new->count = v.count;
    new->next = l;

    return new;
}

int main(int argc, char **argv) {
    int sockfd;                    /* socket */
    int portno;                    /* port to listen on */
    struct sockaddr_in serveraddr; /* server's addr */
    struct hostent *server = NULL;
    struct sockaddr_in clientaddr; /* client addr */
    char buf[BUFSIZE];             /* message buf */
    int optval;                    /* flag value for setsockopt */
    int n;                         /* message byte size */
    fdl_t *fd_list = NULL;

    /*
     * check command line arguments
     */
    if (argc != 3) {
        fprintf(stderr, "usage: %s <localaddr> <port>\n", argv[0]);
        exit(1);
    }

    if (strcmp(argv[1], "INADDR_ANY")) {
        server = gethostbyname(argv[1]);
    }
    portno = atoi(argv[2]);

    /*
     * socket: create the parent socket
     */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    /* setsockopt: Handy debugging trick that lets
     * us rerun the server immediately after we kill it;
     * otherwise we have to wait about 20 secs.
     * Eliminates "ERROR on binding: Address already in use" error.
     */
    optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int));

    /*
     * build the server's Internet address
     */
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;

    if (!strcmp(argv[1], "INADDR_ANY")) {
        serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    } else {
        bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    }
    serveraddr.sin_port = htons((unsigned short)portno);

    /*
     * bind: associate the parent socket with a port
     */
    if (bind(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
        error("ERROR on binding");

    /*
     * main loop: wait for a datagram, then echo it
     */
    struct sockaddr_in consaddr;
    socklen_t addrlen;
    consaddr.sin_family = AF_INET;
    consaddr.sin_port = htons((unsigned short)portno);
    // consaddr.sin_addr
    bzero(&consaddr.sin_zero, sizeof(consaddr.sin_zero));
    while (1) {

        /*
         * recvfrom: receive a UDP datagram from a client
         */
        bzero(buf, BUFSIZE);
        n = recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *)&clientaddr, &addrlen);
        if (n < 0) {
            error("ERROR in recvfrom");
        } else if (0 == n) {
            fd_list = rm_fd(fd_list, 0, clientaddr.sin_addr.s_addr);
            printf(KGRN "%s: \tRemove address (%u) from list. Connections count (%d).\n" RESET, __func__, clientaddr.sin_addr.s_addr, global_count);
            print_fdl(fd_list);
        } else if (1 == n) {
            printf("Consumer subscription topic: %s\n", buf);
            /*
                        // Search if adress already exists
                        if (search_fd(fd_list, clientaddr.sin_addr.s_addr))
                            continue;
                        // address doesn't exist => insert element in list
            */
            fdl_t e;
            bzero(&e, sizeof(e));
            e.fd = 0;
            e.addr = clientaddr.sin_addr.s_addr;
            memcpy(&e.csockaddr_in, &clientaddr, sizeof(clientaddr));
            e.topic = buf[0];
            e.count = 1;
            fd_list = add_fd(fd_list, e);
            print_fdl(fd_list);
        } else if (1 < n) {
            fdl_t *tmp = fd_list;
            while (tmp) {
                if (tmp->topic == buf[0]) {
                    // Send buffer to associated consumer address
                    int flags = 0;
                    if (sendto(sockfd, buf, strlen(buf), flags, (struct sockaddr *)&tmp->csockaddr_in, sizeof(tmp->csockaddr_in)) < 0) {
                        perror("sendto()");
                    }
                }
                tmp = tmp->next;
            }
        }
    }
    close(sockfd);
}
