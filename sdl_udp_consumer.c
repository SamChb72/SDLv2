//########################################################
//#       Copyright (C) 2018 GreenFLOWS                  #
//#                                                      #
//# This file is part of GreenFLOWS projects and can not #
//# be copied and/or distributed in any medium or format #
//#    without the express permission of GreenFLOWS      #
//#                                                      #
//#              samy.chbinou@greenflows.fr              #
//########################################################

#include "sdl.h"

int subscribe_receive(struct sockaddr_in serveraddr, char *topic, unsigned int msglen, counters_t *counters) {
    char buf[BUFSIZE];
    int nbytes;

    /* socket: create the socket */
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket()");
        return EXIT_FAILURE;
    }

// sockopt(sockfd, 19 + msglen);

#if 0
    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) {
        perror("connect() to brocker");
        close(sockfd);
        return EXIT_FAILURE;
    }
    printf("Connected to brocker !!! Subscribing to %s\n", topic);
#endif

    /* Subscription */
    int flags = 0; // | MSG_NOSIGNAL;
    char tpid = topic_id(topic);
    char hostname[32];
    bzero(&hostname, sizeof(hostname));
    FILE *dockerf;
    dockerf = fopen("/proc/1/cpuset", "r");
    if (dockerf) {
        char line[128];
        if (!fgets(line, 128, dockerf))
            gethostname(hostname, sizeof(hostname));
        else {
            int len = strlen(line);
            if (len < 21) {
                gethostname(hostname, sizeof(hostname));
            } else {
                strncpy(hostname, line + 8, 12);
            }
        }
        fclose(dockerf);
    } else {
        gethostname(hostname, sizeof(hostname));
    }
    nbytes = sendto(sockfd, &tpid, 1, flags, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
    // printf("n=%d\n", n);
    if (nbytes < 0) {
        printf("Subscription failed. ");
        perror("sendto()");
        close(sockfd);
        return EXIT_FAILURE;
    }

    printf("Waiting data from brocker\n");
    for (;;) {
        bzero(buf, BUFSIZE);
        int recvflags = O_NONBLOCK; // | MSG_DONTWAIT;
        nbytes = recvfrom(sockfd, buf, sizeof(buf), recvflags, NULL, NULL);
        if (nbytes < 0) {
            perror("recvfrom()");
            break;
        }
        print_rcv_stats(hostname, counters, msglen, buf, 100000);
        if (19 + 4 == nbytes) {
            printf("RECEIVED STOP MSG\n");
            // TODO: pb in case of many consumers
            // sendto(sockfd, "", 0, flags, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
            break;
        }
    }
    close(sockfd);
    return EXIT_FAILURE;
}

int main(int argc, char **argv) {
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    int portno;
    char *topic;
    unsigned int msglen;
    counters_t counters;
    bzero(&counters, sizeof(counters));
    counters.min = 999999;

    /* check command line arguments */
    if (argc != 5) {
        fprintf(stderr, "usage: %s <hostname> <port> <topic> <msglen>\n", argv[0]);
        fprintf(stderr, "ex: %s 127.0.0.1 65111 MICRO_SERVICES_INFO 256\n", argv[0]);
        exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);
    topic = argv[3];
    msglen = atoi(argv[4]);

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        perror("gethostbyname()");
        exit(EXIT_FAILURE);
    }

    /* portno */
    if (portno <= 49151) {
        printf("This port number (%d) is reserved and cant be used.\nport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }
    if (portno >= 65535) {
        printf("This port number (%d) is too big.\nnport must be 49151<port<65535", portno);
        exit(EXIT_FAILURE);
    }

    /* topic */
    if (strlen(topic) > 64) {
        printf("Please use a topic name less than 64 characters long\n");
        exit(EXIT_FAILURE);
    }

    /* build the server's Internet address */
    bzero((char *)&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    int ret = subscribe_receive(serveraddr, topic, msglen, &counters);
    while (0 != ret) {
        // Retry every second
        usleep(1000 * 1000);
        ret = subscribe_receive(serveraddr, topic, msglen, &counters);
    }
    return EXIT_SUCCESS;
}
