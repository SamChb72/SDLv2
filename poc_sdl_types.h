/*
########################################################
#       Copyright (C) 2018 GreenFLOWS                  #
#                                                      #
# This file is part of GreenFLOWS projects and can not #
# be copied and/or distributed in any medium or format #
#    without the express permission of GreenFLOWS      #
#                                                      #
#            samy.chbinou@greenflows.fr                #
########################################################
*/

/* Shared Data Layer for Microservices communications */

#ifndef sdl_types_h__
#define sdl_types_h__

#include <netdb.h>
#include <string.h>
#include <time.h>

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

// Add some colors:
#define GRN "\x1B[32m"
#define BLU "\x1B[34m"
#define RESET "\x1B[0m"

typedef struct producer_t producer_t;
struct producer_t {
    int id;
    unsigned long addr;
    struct sockaddr_in csockaddr_in;
    unsigned int count;
    unsigned char topic;
    producer_t *next;
};
producer_t *producer_list;

typedef struct counts_t counts_t;
struct counts_t {
    unsigned int _0_15;
    unsigned int _15_25;
    unsigned int _25_50;
    unsigned int _50_75;
    unsigned int _75_100;
    unsigned int _100_500;
    unsigned int _500_1000;
    unsigned int _1000p;
};

typedef struct stats_t stats_t;
struct stats_t {
    unsigned long long sum;
    unsigned long long sum2;
    unsigned int total;
    unsigned int latency;
    unsigned int sum_1k;
    unsigned int sum2_1k;
    unsigned int latency_1k[1000];
    unsigned int sum_10k;
    unsigned int sum2_10k;
    unsigned int latency_10k[10000];
    unsigned int sum_100k;
    unsigned int sum2_100k;
    unsigned int latency_100k[100000];
    unsigned int min;
    unsigned int max;
    unsigned int size_errors;
    unsigned int sum_size;
    struct timeval ts;
    //    float avg;
    //    float sig2;
    //    float bw;
    counts_t cnt;
};

typedef struct consume_t consume_t;
struct consume_t {
    int id;
    int thread_id;
    int sock_id;
    stats_t stats;
    char *table;
    consume_t *next;
};

unsigned char topic_id(char *topic) {
    // GUI
    if (!strcmp("UI_NS_VNF_ORCHESTRATION", topic))
        return '1';
    else if (!strcmp("UI_NS_VNC_SETTING", topic))
        return '2';
    else if (!strcmp("UI_SS_TRAFFIC_PROFILE", topic))
        return '3';
    else if (!strcmp("UI_CONTROL_SCREEN_SETTINGS", topic))
        return '4';
    // SWARM
    else if (!strcmp("MICRO_SERVICES_INFO", topic))
        return '5';
    // LRM
    else if (!strcmp("LRM_DB_MEAS_IND", topic))
        return '6';
    // BBSC
    else if (!strcmp("BBSC_DB_REG_FEU_EC_READY", topic))
        return '7';
    else if (!strcmp("BBSC_DB_DRB_ALLOCATION_AND_SPLIT", topic))
        return '8';
    // MHNI
    else if (!strcmp("XHAUL_NETWORK_INFO", topic))
        return '9';
    else if (!strcmp("FZM_INFO", topic))
        return 'a';
    // TRAFFIC
    else if (!strcmp("MBB_TRAFFIC_PROFILE_INFO", topic))
        return 'b';
    else if (!strcmp("IoT_TRAFFIC_PROFILE_INFO", topic))
        return 'c';
    // GUI CELL VM
    else if (!strcmp("CELL_VM_INFO", topic))
        return 'd';
    else if (!strcmp("RESOURCE_INFO", topic))
        return 'e';
    else
        return '0';
}

#endif // sdl_types_h__
